/* 
 * File:   fib.c
 * Author: joy
 *
 * Created on 30 August, 2015, 7:21 PM
 */

/************************************ Simple recursive version to find nth fibonacci number.. *****************************************************/
#include <stdio.h>
#include <stdlib.h>

/******** function declarations. ******/
int fib(int n);

/*
 * 
 */
int main(int argc, char** argv) {

    int n;
    printf("Enter the value of n:");
    scanf("%d",&n);
    int result = fib(n);
    printf("Value of fibonacci number is: %d",result);
    return (EXIT_SUCCESS);
}

/**
 * 
 * @param n the fibonacci term required.
 * @return nth fibonacci term.
 */
int fib(int n){
    if(n <= 1)
        return n;
    return fib(n-1) + fib(n-2);
}