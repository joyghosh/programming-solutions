/*************************************************************************************************************
Program to find possible permutations of a given string.
e.g. ABC, ACB, BAC, BCA, CAB, CBA
*************************************************************************************************************/
#include <stdio.h>
#include <string.h>

/**************** This section contains all the declarations *************************/
void swap(char* x, char* y);
void permute(char *s, int l, int r);


/**************** This section consists of the driver main function *****************/
int main(){

    char str[] = "ABC";
    int n = strlen(str);
    permute(str, 0, n-1);
    getchar();
    return 0;
}

/****************** This section contains all the definitions ***********************/
void swap(char *x, char *y){
    char temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

void permute(char *s, int l, int r){

    if(l == r){
        printf("%s\n",s);
    }else{

        int i;
        for(i=l; i<=r; i++){
            swap((s+l),(s+i));
            permute(s, l+1, r);
            swap((s+l),(s+i));      //backtraking.
        }
    }
}
