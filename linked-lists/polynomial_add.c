/*************************************************************************************************************
-------------------------- Program to add two polynomials represented as linked lists ------------------------
*************************************************************************************************************/
#include <stdio.h>
#include <malloc.h>


/*************************************************************************************************************
 ----------------------- This section contains the custom type and function declarations. --------------------
 ************************************************************************************************************/
struct node{
    float coeffecient;
    int exponential;
    struct node *link;
};

typedef struct node *NODE;

NODE insert(NODE head, float coefficient, int exponential);
NODE add_polynomials(NODE poly1, NODE poly2);
NODE enter(NODE head);
void display(NODE head);


/*************************************************************************************************************
 ----------------------- This section contains the main execution point of program. -------------------------
 ************************************************************************************************************/
int main(){

    NODE poly1_head, poly2_head, poly3_head;
    poly1_head = poly2_head = poly3_head = NULL;

    printf("Enter polynomials\n");
    printf("\nPolynomial 1: ");
    poly1_head = enter(poly1_head);
    printf("\nPolynomial 2: ");
    poly2_head = enter(poly2_head);

    poly3_head = add_polynomials(poly1_head, poly2_head);
    printf("Polynomial 1 is: ");
    display(poly1_head);
    printf("Polynomial 2 is: ");
    display(poly2_head);
    printf("Added polynomial: ");
    display(poly3_head);

    getchar();
    return 0;
}

/*************************************************************************************************************
 ----------------------- This section contains the function definitions. ------------------------------------
*************************************************************************************************************/
NODE insert(NODE head, float coefficient, int exponential){

        NODE ptr, temp;

        temp = (NODE)malloc(sizeof(struct node));
        temp->coeffecient = coefficient;
        temp->exponential = exponential;

        //Terms in polynomials occur in descending order of their exponentials.
        if(head == NULL || exponential > head->exponential){
            temp->link = head;
            head = temp;                //setting the head.
        }else{
            ptr = head;
            while(ptr->link != NULL && ptr->link->exponential > exponential)
                ptr = ptr->link;

            temp->link = ptr->link;
            ptr->link = temp;
            if(ptr->link == NULL)
                temp->link = NULL;
        }

        return head;
}

NODE add_polynomials(NODE poly1, NODE poly2){
    NODE poly3_head, poly3, temp;
    poly3_head = NULL;

    if(poly1 == NULL && poly2 == NULL)
        return poly3_head;

    while(poly1!=NULL && poly3!=NULL){
        temp = (NODE)malloc(sizeof(struct node));

        if(poly3_head == NULL){
            poly3_head = temp;
            poly3 = poly3_head;
        }else{
            poly3->link = temp;
            poly3 = poly3->link;
        }

        //Actual exponential power checking.
        if(poly1->exponential > poly2->exponential){
            temp->coeffecient = poly1->coeffecient;
            temp->exponential = poly1->exponential;
            poly1 = poly1->link;
        }else if (poly2->exponential > poly1->exponential) {
            temp->coeffecient = poly2->coeffecient;
            temp->exponential = poly2->exponential;
            poly2 = poly2->link;
        }else if (poly1->exponential == poly2->exponential) {
            temp->coeffecient = poly1->coeffecient + poly2->coeffecient;
            temp->exponential = poly1->exponential;
            poly1 = poly1->link;  poly2 = poly2->link;
        }
    }

    while(poly1 != NULL){
        temp = (NODE)malloc(sizeof(struct node));
        temp->coeffecient = poly1->coeffecient;
        temp->exponential = poly1->exponential;

        if(poly3_head == NULL){
            poly3_head = temp;
            poly3 = poly3_head;
        }else{
            poly3->link = temp;
            poly3 = poly3->link;
        }

        poly1 = poly1->link;
    }

    while(poly2 != NULL){
        temp = (NODE)malloc(sizeof(struct node));
        temp->coeffecient = poly2->coeffecient;
        temp->exponential = poly2->exponential;

        if(poly3_head == NULL){
            poly3_head = temp;
            poly3 = poly3_head;
        }else{
            poly3->link = temp;
            poly3 = poly3->link;
        }

        poly2 = poly2->link;
    }

    return poly3_head;
}

NODE enter(NODE head){

    int i, n, exponential;
    float coefficient;

    printf("How many terms do you want to enter? ");
    scanf("%d",&n);

    for(i=1; i<=n; i++){
        printf("Enter the coefficient for term %d: ",i);
        scanf("%f",&coefficient);

        printf("Enter the exponential for term %d: ",i);
        scanf("%d",&exponential);

        head = insert(head, coefficient, exponential);
    }

    return head;
}

void display(NODE head){
    if(head == NULL){
        printf("Empty\n");
        return;
    }

    while(head!=NULL){
        printf("%.1fx^%d + ",head->coeffecient, head->exponential);
        head = head->link;
    }
}
