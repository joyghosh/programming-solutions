/*************************************************************************************************************
-------------------------- Program to convert from decimal to hexadecimal------------------------------------
*************************************************************************************************************/
#include <stdio.h>

/*************************************************************************************************************
 ----------------------- This section contains the custom type and function declarations. --------------------
*************************************************************************************************************/
void convertToHexadecimal(long int n);
/*************************************************************************************************************
 ----------------------- This section contains the main execution point of program. -------------------------
*************************************************************************************************************/
int main()
{
    int long n;
    printf("Enter the decimal number to convert: ");
    scanf("%ld",&n);
    convertToHexadecimal(n);
    printf("\n");
    getchar();
    return 0;
}


/*************************************************************************************************************
 ----------------------- This section contains the function definitions. ------------------------------------
*************************************************************************************************************/
void convertToHexadecimal(long n){
    long int remainder, quotient;
    int i=1, j;
    char hexadecimalNumber[100];

    quotient = n;
    while(quotient!=0){
        remainder = quotient%16;

        if(remainder<10)
            remainder = remainder+48;
        else
            remainder = remainder+55;
        hexadecimalNumber[i++] = remainder;
        quotient=quotient/16;
    }

    for(j=i-1;j>0;j--){
        printf("%c",hexadecimalNumber[j]);
    }
}
