/*************************************************************************************************************
-------------------------- Program to efficiently find if a number is multiple of 3 -------------------------
*************************************************************************************************************/
#include <stdio.h>

/*************************************************************************************************************
 ----------------------- This section contains the custom type and function declarations. --------------------
 ************************************************************************************************************/
int isMultipleOf3(int n);

/*************************************************************************************************************
 ----------------------- This section contains the main execution point of program. -------------------------
 ************************************************************************************************************/
int main(){
    int N = 1;
    if(isMultipleOf3(N))
        printf("%d is a multiple of 3",N);
    else
        printf("%d is not a multiple of 3",N);
    getchar();
    return 0;
}

/*************************************************************************************************************
 ----------------------- This section contains the function definitions. ------------------------------------
*************************************************************************************************************/
int isMultipleOf3(int n){

    if(n < 0)                           //check if the number is negative, if yes change it to positive.
        n = -n;

    if(n == 0)                          //check for zero or one.
        return 1;
    else if(n == 1)
        return 0;

    int odd_count, even_count;          //counters for odd and even set bits.
    odd_count = even_count = 0;

    while(n!=0){
        if(n&1)
            ++odd_count;                //increment odd counter.
        n = (n>>1);                    //right shift by 1.
        if(n&1)
            ++even_count;                //increment even counter.
        n = (n>>1);                    //right shift by 1.
    }

    return isMultipleOf3(odd_count-even_count);
}
